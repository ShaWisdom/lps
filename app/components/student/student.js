var studentModule = angular.module('studentModule', []);

studentModule.controller('StudentController', ['$scope', 'StudentService', '$rootScope', '$filter', '$state', 'message', 'SERVICE_END_POINTS', 'DTOptionsBuilder', '$window', 'AuthenticationFactory', function ($scope, StudentService, $rootScope, $filter, $state, message, SERVICE_END_POINTS, DTOptionsBuilder, $window, AuthenticationFactory) {
    $scope.imageUpload = function () {
        $('#image_upload_input').click();
    };

    $scope.gotoTab = function (tabName) {
        switch (tabName.toLowerCase()) {
        case $scope.tabsList[0].name.toLowerCase():
            $scope.selectedStudentTab = 0;
            break;
        case $scope.tabsList[1].name.toLowerCase():
            $scope.selectedStudentTab = 1;
            break;
        }
    }

    if ($rootScope.CurrentUser) {
        userSecurityGroups = AuthenticationFactory.CurrentUser() ? AuthenticationFactory.CurrentUser().userSecurityGroups : null;

        $scope.courseMap = AuthenticationFactory.CurrentUser() ? AuthenticationFactory.CurrentUser().courseMap : null;
        $scope.classroomMap = AuthenticationFactory.CurrentUser() ? AuthenticationFactory.CurrentUser().classroomMap : null;

        $scope.isWriteAcccess = userSecurityGroups ? userSecurityGroups.indexOf('STUDENT_PROFILE_READ_WRITE') ? true : false : false;

        $scope.isReadOnlyOrWriteAcccess = userSecurityGroups ? userSecurityGroups.indexOf('STUDENT_PROFILE_READ_WRITE') || userSecurityGroups.indexOf('STUDENT_PROFILE_READ_ONLY') ? true : false : false;

        //$scope.isReadOnlyOrWriteAcccess = false;

        $rootScope.schoolCd = AuthenticationFactory.CurrentUser() ? AuthenticationFactory.CurrentUser().schoolCd : null;

        $scope.studentId = AuthenticationFactory.CurrentUser() ? AuthenticationFactory.CurrentUser().employeeProfileId : null; // $rootScope.studentId ? $rootScope.studentId : 2;
    }

    var clearStudentSearchData = function () {
        var isStudentList = JSON.parse($window.localStorage.getItem('searchList'));
        if (isStudentList) {
            $window.localStorage.removeItem('searchList');
        }
    };

    clearStudentSearchData();

    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withPaginationType('full_numbers')
        .withDisplayLength(10)
        .withBootstrap();

    $scope.tabsList = [
        {
            "id": 1,
            "route": "student.search",
            "name": "Search"
        },
        {
            "id": 2,
            "route": "student.profile",
            "name": "Profile"
        },
        {
            "id": 3,
            "route": "student.parentdetails",
            "name": "Parent's Details"
        },
        {
            "id": 4,
            "route": "student.fees",
            "name": "Fees"
        },
        {
            "id": 5,
            "route": "student.attendance",
            "name": "Attendence"
        },
        {
            "id": 6,
            "route": "student.marks",
            "name": "Marks"
        },
        {
            "id": 7,
            "route": "student.previouseducationdetails",
            "name": "Previous Education Details"
        },
        {
            "id": 8,
            "route": "student.feedback",
            "name": "Feedback"
        }];

    if ($scope.isReadOnlyOrWriteAcccess) {
        $scope.selectedStudentTab = 0;
    } else {
        $scope.selectedStudentTab = 1;
    }

    $rootScope.loading = true;

    $scope.uploadFile = function (files) {
        var studentId = $scope.studentId;
        $rootScope.loading = true;

        StudentService.imageUpload(studentId).then(function (response) {
            StudentService.uploadImageUsingMetadata(files[0], response.data).then(function (metaDataResponse) {
                $rootScope.loading = false;
                message.success('Image Uploaded', 'The image has been uploaded successfully.');
            }, function () {
                $rootScope.loading = false;
                message.error('Image upload failed', 'Cannot upload the image now. Please try again.');
            });
        }, function () {
            $rootScope.loading = false;
            message.error('Image loading failed', 'Cannot load the image now. Please try again.');
        });
    };

    $scope.loadStudentData = function (studentId) {
        $scope.selectedStudentTab = 1;
        $scope.studentId = studentId;
        $rootScope.goToRoute('student.profile');
    }

    /* Create Student */
    $scope.setupCreateStudent = function (tagName) {
            $rootScope.loading = false;
            switch (tagName) {
            case 'student':
                $scope.studentId = null;
                $scope.commonData = {};
                $scope.userData = {};
                $scope.studentData = {};
                break;
            case 'parent':
                $scope.userData = {};
                $scope.parentData = {};
                break;
            }
        }
        /* End Create Student */

}]);

studentModule.service('StudentService', ['$http', '$rootScope', 'BASE_PATH', 'SERVICE_END_POINTS', function ($http, $rootScope, BASE_PATH, SERVICE_END_POINTS) {
    var studentData = {};
    studentData.getStudentDataById = function (studentId) {
        return $http({
            method: 'GET',
            url: BASE_PATH + SERVICE_END_POINTS.student.profile.replace("<studentId>", studentId).replace("<schoolCd>", $rootScope.schoolCd)
        });
    };

    studentData.updateStudentById = function (studentId, studentDetails) {
        return $http({
            method: 'PUT',
            url: BASE_PATH + SERVICE_END_POINTS.student.profile.replace("<studentId>", studentId).replace("<schoolCd>", $rootScope.schoolCd),
            data: studentDetails
        });
    };

    studentData.createStudent = function (studentDetails) {
        return $http({
            method: 'POST',
            url: BASE_PATH + SERVICE_END_POINTS.student.profile.replace("<studentId>", '').replace("<schoolCd>", $rootScope.schoolCd),
            data: studentDetails
        });
    };

    studentData.removeStudentById = function (studentId) {
        return $http({
            method: 'DELETE',
            url: BASE_PATH + SERVICE_END_POINTS.student.profile.replace("<studentId>", studentId).replace("<schoolCd>", $rootScope.schoolCd)
        });
    };

    studentData.imageUpload = function (studentId) {
        return $http({
            method: 'GET',
            url: BASE_PATH + SERVICE_END_POINTS.image.imageUpload.replace("<imageFileType.student>", $rootScope.imageFileType.student).replace("<studentId>", studentId).replace("<schoolCd>", $rootScope.schoolCd)
        });
    };

    studentData.uploadImageUsingMetadata = function (file, inputData) {
        var dataList = {
            'AWSAccessKeyId': inputData.awsAccessKeyId,
            'key': inputData.key,
            'policy': inputData.policy,
            'signature': inputData.signature,
            'acl': 'private'
        };
        return $http({
            method: 'POST',
            url: inputData.formAction,
            headers: {
                'Content-Type': undefined
            },
            transformRequest: function (data) {
                var formData = new FormData();
                formData.append("model", angular.toJson(dataList));
                formData.append("file", file);
                return formData;
            },
            data: {
                model: dataList,
                files: file
            }
        });
    };

    studentData.studentGetParentProfilesByStudentId = function (studentId) {
        return $http({
            method: 'GET',
            url: BASE_PATH + SERVICE_END_POINTS.student.parentDetails.replace("<studentId>", studentId).replace("<parentId>", '').replace("<schoolCd>", $rootScope.schoolCd)
        });
    };

    studentData.updateParentByStudentAndParentId = function (studentId, parentId, parentDetails) {
        return $http({
            method: 'PUT',
            url: BASE_PATH + SERVICE_END_POINTS.student.parentDetails.replace("<studentId>", studentId).replace("<parentId>", parentId).replace("<schoolCd>", $rootScope.schoolCd),
            data: parentDetails
        });
    };

    studentData.createParentByStudentId = function (studentId, parentDetails) {
        return $http({
            method: 'POST',
            url: BASE_PATH + SERVICE_END_POINTS.student.parentDetails.replace("<studentId>", studentId).replace("<parentId>", '').replace("<schoolCd>", $rootScope.schoolCd),
            data: parentDetails
        });
    };

    studentData.deleteParentByStudentIdAndParentId = function (studentId, parentId) {
        return $http({
            method: 'DELETE',
            url: BASE_PATH + SERVICE_END_POINTS.student.parentDetails.replace("<studentId>", studentId).replace("<parentId>", parentId).replace("<schoolCd>", $rootScope.schoolCd),
        });
    };

    studentData.getStates = function () {
        return $http({
            method: 'GET',
            url: BASE_PATH + SERVICE_END_POINTS.utililty.getStates
        });
    };

    studentData.getStudentsListByCourse = function (courseId) {
        return $http({
            method: 'GET',
            url: BASE_PATH + SERVICE_END_POINTS.student.search.course.replace("<courseId>", courseId).replace("<schoolCd>", $rootScope.schoolCd)
        });
    };

    studentData.getStudentsListByClassroom = function (courseId, classroomId) {
        return $http({
            method: 'GET',
            url: BASE_PATH + SERVICE_END_POINTS.student.search.classroom.replace("<courseId>", courseId).replace("<classroomId>", classroomId).replace("<schoolCd>", $rootScope.schoolCd)
        });
    };

    studentData.getStudentsListByKeyword = function (keyword) {
        return $http({
            method: 'GET',
            url: BASE_PATH + SERVICE_END_POINTS.student.search.keyword.replace("<keyword>", keyword).replace("<schoolCd>", $rootScope.schoolCd)
        });
    };

    studentData.getClassroomsByCourse = function (courseId) {
        return $http({
            method: 'GET',
            url: BASE_PATH + SERVICE_END_POINTS.student.search.classroom.replace("<courseId>", courseId).replace("<classroomId>/students", '').replace("<schoolCd>", $rootScope.schoolCd)
        });
    };

    return studentData;
}]);

studentModule.directive('studentProfile', ['StudentService', '$rootScope', '$filter', 'message', function (StudentService, $rootScope, $filter, message) {
    return {
        restrict: 'EA',
        link: function (scope, ele, attr) {
            studentProfile(scope, ele, attr, StudentService, $rootScope, $filter, message);
        }
    }
}]);

studentModule.directive('studentSearch', ['StudentService', '$rootScope', '$filter', 'message', '$window', function (StudentService, $rootScope, $filter, message, $window) {
    return {
        restrict: 'EA',
        link: function (scope, ele, attr) {
            studentSearch(scope, ele, attr, StudentService, $rootScope, $filter, message, $window);
        }
    }
}]);

studentModule.directive('studentParentProfile', ['StudentService', '$rootScope', '$filter', 'message', function (StudentService, $rootScope, $filter, message) {
    return {
        restrict: 'EA',
        link: function (scope, ele, attr) {
            studentParentProfile(scope, ele, attr, StudentService, $rootScope, $filter, message);
        }
    }
}]);

studentModule.directive('studentCreate', ['StudentService', '$rootScope', '$filter', 'message', function (StudentService, $rootScope, $filter, message) {
    return {
        restrict: 'EA',
        link: function (scope, ele, attr) {
            studentCreate(scope, ele, attr, StudentService, $rootScope, $filter, message);
        }
    }
}]);

studentModule.directive('studentParentCreate', ['StudentService', '$rootScope', '$filter', 'message', function (StudentService, $rootScope, $filter, message) {
    return {
        restrict: 'EA',
        link: function (scope, ele, attr) {
            studentParentCreate(scope, ele, attr, StudentService, $rootScope, $filter, message);
        }
    }
}]);

studentProfile = function (scope, ele, attr, StudentService, $rootScope, $filter, message) {
    $rootScope.currentPageName = "Student Profile";
    scope.user = {};
    scope.userData = {};
    scope.commonData = {};

    scope.commonData.isStudentDisabled = true;

    scope.editStudent = function () {
        scope.commonData.isStudentDisabled = false;
    };

    scope.deleteStudent = function (studentId) {
        if (confirm("Are you sure to delete this?")) {
            $rootScope.loading = true;
            StudentService.removeStudentById(studentId).then(function (response) {
                $rootScope.loading = false;
                message.success('Removed Successfully', 'The student has been removed.');
            }, function (err) {
                $rootScope.loading = false;
                message.error('Deletion Failed', 'Cannot Remove Student now. Pease try again.');
            });
        }
    };

    scope.updateStudent = function (isValid) {
        if (isValid && !!$('#studentDOB').val() && !!$('#studentDOJ').val()) {
            $rootScope.loading = true;
            scope.studentData.gender = scope.userData.gender == 0 ? "Male" : "Female";
            scope.studentData.dateOfBirth = $rootScope.dateFilterBackend($('#studentDOB').val());
            scope.studentData.dateOfJoining = $rootScope.dateFilterBackend($('#studentDOJ').val());

            scope.studentData.isActive = scope.userData.isActive;

            scope.studentData.courseId = $rootScope.findKeyInListByValue(scope.$parent.courseMap, scope.userData.courseId) ? $rootScope.findKeyInListByValue(scope.$parent.courseMap, scope.userData.courseId) : scope.userData.courseId;
            scope.studentData.classroomId = $rootScope.findKeyInListByValue(scope.$parent.classroomMap, scope.userData.classroomId) ? $rootScope.findKeyInListByValue(scope.$parent.classroomMap, scope.userData.classroomId) : scope.userData.classroomId;

            StudentService.updateStudentById(scope.studentId, scope.studentData).then(function (response) {
                $rootScope.loading = false;
                message.success('Update Successful', 'The user data has been updated successfully!');
            }, function (data) {
                $rootScope.loading = false;
                message.error('Update Failed', 'Cannot update the details. Please try again.');
            });
            scope.commonData.isStudentDisabled = true;
        } else {
            message.error('Form Incomplete', 'Please fill all the required fields');
        }
    }

    scope.getStudentById = function () {
        $rootScope.currentPageName = "Student Profile";
        scope.studentId = scope.$parent.studentId;
        scope.userData = {};

        $rootScope.loading = true;
        StudentService.getStudentDataById(scope.studentId).then(function (data) {
            scope.studentData = data.data;
            scope.userData.courseId = $rootScope.findValueInListByKey(scope.$parent.courseMap, scope.studentData.courseId) ? $rootScope.findValueInListByKey(scope.$parent.courseMap, scope.studentData.courseId) : scope.studentData.courseId;
            scope.userData.classroomId = $rootScope.findValueInListByKey(scope.$parent.classroomMap, scope.studentData.classroomId) ? $rootScope.findValueInListByKey(scope.$parent.classroomMap, scope.studentData.classroomId) : scope.studentData.classroomId;
            scope.userData.createdTs = $filter('date')(scope.studentData.createdTs, 'MM/dd/yyyy, H:mm:ss');
            scope.userData.updatedTs = $filter('date')(scope.studentData.updatedTs, 'MM/dd/yyyy, H:mm:ss');
            $rootScope.loading = true;

            scope.studentData.dateOfJoining = $rootScope.dateFilterFrontend(scope.studentData.dateOfJoining);
            scope.studentData.dateOfBirth = $rootScope.dateFilterFrontend(scope.studentData.dateOfBirth);

            scope.userData.gender = scope.studentData.gender ? scope.studentData.gender.toLowerCase() == "female" ? 1 : 0 : null;
            scope.userData.isActive = scope.studentData.isActive;
            $rootScope.loading = false;
        }, function (data) {
            $rootScope.loading = false;
            message.error('Retrieval Failed', 'Cannot load student data due to an error. Please try again.');
        });
    }

    scope.toggleActivateStudent = function (selection) {
        switch (selection) {
        case 'N':
            scope.userData.isActive = 'N';
            break;
        case 'Y':
            scope.userData.isActive = 'Y';
            break;
        }
    }

    scope.getStudentById();
}

studentCreate = function (scope, ele, attr, StudentService, $rootScope, $filter, message) {
    $rootScope.currentPageName = "Add New Student";

    $rootScope.loading = false;
    scope.userData = {};
    scope.commonData = {};
    scope.studentData = {};

    scope.studentData = {
        "rollNbr": "LOTUS1A2",
        "courseId": 14,
        "classroomId": 17,
        "nationality": "INDIA",
        "schoolCd": "LOTUS"
    };

    scope.toggleActivateStudent = function (selection) {
        switch (selection) {
        case 'N':
            scope.userData.isActive = 'N';
            break;
        case 'Y':
            scope.userData.isActive = 'Y';
            break;
        }
    };

    scope.toggleActivateStudent('N');

    scope.userData.courseId = $rootScope.findValueInListByKey(scope.$parent.courseMap, scope.studentData.courseId) ? $rootScope.findValueInListByKey(scope.$parent.courseMap, scope.studentData.courseId) : scope.studentData.courseId;
    scope.userData.classroomId = $rootScope.findValueInListByKey(scope.$parent.classroomMap, scope.studentData.classroomId) ? $rootScope.findValueInListByKey(scope.$parent.classroomMap, scope.studentData.classroomId) : scope.studentData.classroomId;

    scope.saveStudent = function (isValid) {
        if (isValid && !!$('#studentDOB').val() && !!$('#studentDOJ').val()) {
            $rootScope.loading = true;
            scope.studentData.isActive = scope.userData.isActive;
            scope.studentData.dateOfBirth = $rootScope.dateFilterBackend($('#studentDOB').val());
            scope.studentData.dateOfJoining = $rootScope.dateFilterBackend($('#studentDOJ').val());

            scope.studentData.courseId = $rootScope.findKeyInListByValue(scope.$parent.courseMap, scope.userData.courseId) ? $rootScope.findKeyInListByValue(scope.$parent.courseMap, scope.userData.courseId) : scope.userData.courseId;
            scope.studentData.classroomId = $rootScope.findKeyInListByValue(scope.$parent.classroomMap, scope.userData.classroomId) ? $rootScope.findKeyInListByValue(scope.$parent.classroomMap, scope.userData.classroomId) : scope.userData.classroomId;

            scope.studentData.gender = scope.userData.gender == 0 ? "Male" : "Female";
            StudentService.createStudent(scope.studentData).then(function (response) {
                $rootScope.loading = false;
                message.success('Created Successfully', 'The student entry has been created.');
                $rootScope.goToRoute('student.profile');
            }, function (data) {
                $rootScope.loading = false;
                message.error('Creation Failed', 'Cannot add new student due to an error. Please try again.');
            });
        } else {
            message.error('Form Incomplete', 'Please fill out all the mandatory fields');
        }
    };
}

studentParentCreate = function (scope, ele, attr, StudentService, $rootScope, $filter, message) {
    $rootScope.currentPageName = "Add New Parent";

    $rootScope.loading = false;
    scope.commonData = {};
    scope.userData = {};
    scope.parentData = {};
    scope.parentData.studentProfileId = scope.$parent.studentId;

    scope.userData.isActive = 'N';

    scope.toggleActivate = function (selection) {
        switch (selection) {
        case 'N':
            scope.userData.isActive = 'N';
            break;
        case 'Y':
            scope.userData.isActive = 'Y';
            break;
        }
    };

    StudentService.getStates().then(function (response) {
        scope.stateList = [];
        angular.forEach(response.data, function (value, key) {
            scope.stateList.push(value);
        });
    }, function (error) {
        message.error('State list details load Failed', 'Cannot get the State details. Please try again.');
    });

    scope.saveParent = function (isValid, studentId) {
        if (isValid) {
            $rootScope.loading = true;
            scope.parentData.isActive = scope.userData.isActive;
            scope.parentData.schoolCd = "LOTUS";

            StudentService.createParentByStudentId(studentId, scope.parentData).then(function (response) {
                $rootScope.loading = false;
                message.success('Created Successfully', 'The new parent information has been added.');
                $rootScope.goToRoute('student.parentdetails');
            }, function (errorData) {
                $rootScope.loading = false;
                message.error('Creation Failed', 'Cannot create new Parent now. Please try again.');
            });
        } else {
            message.error('Form Incomplete', 'Please fill out all the mandatory fields');
        }
    }
}

studentParentProfile = function (scope, ele, attr, StudentService, $rootScope, $filter, message) {
    $rootScope.currentPageName = "Parent's Details";

    $rootScope.loading = true;
    scope.parentList = {};
    scope.commonData = {};
    scope.userData = {};
    scope.commonData.isParentDisabled = true;

    scope.clearParentDetails = function () {
        scope.parentList = scope.parentData = scope.userData = null;
        scope.commonData.isParentDisabled = true;
    }

    scope.clearParentDetails();

    StudentService.getStates().then(function (response) {
        scope.stateList = [];
        angular.forEach(response.data, function (value, key) {
            scope.stateList.push(value);
        });
    }, function (error) {
        message.error('State list details load Failed', 'Cannot get the State details. Please try again.');
    });

    scope.studentId = scope.$parent.studentId;
    scope.getParentList = function (studentId) {
        $rootScope.loading = true;
        StudentService.studentGetParentProfilesByStudentId(studentId).then(function (response) {
            $rootScope.loading = false;
            if (response.status == 200) {
                scope.parentList = response.data;
            } else {
                message.error(response.statusText ? response.statusText : 'Cannot find the related data', 'The related data either doesn\'t exists or cannot be found.');
            }
        }, function (err) {
            $rootScope.loading = false;
            message.error('Retrieval Failed', 'Cannot retrive parent details now. Please try again.');
        });
    };

    scope.getParentList(scope.studentId);

    scope.loadParentData = function (parentId) {
        scope.selectedParentId = parentId;
        scope.userData = {};
        scope.parentData = $filter('filter')(scope.parentList, {
            parentId: parentId
        })[0];
        scope.userData.createdTs = $filter('date')(scope.parentData.createdTs, 'MM/dd/yyyy, H:mm:ss');
        scope.userData.updatedTs = $filter('date')(scope.parentData.updatedTs, 'MM/dd/yyyy, H:mm:ss');
    }

    scope.toggleActivate = function (selection) {
        switch (selection) {
        case 'N':
            scope.userData.isActive = 'N';
            break;
        case 'Y':
            scope.userData.isActive = 'Y';
            break;
        }
    };


    scope.deleteParent = function (studentId, parentId) {
        if (confirm("Are you sure to delete this entry?")) {
            $rootScope.loading = true;
            scope.clearParentDetails();
            StudentService.deleteParentByStudentIdAndParentId(studentId, parentId).then(function (response) {
                $rootScope.loading = false;
                scope.clearParentDetails();
                scope.getParentList(studentId);
                //$rootScope.modal('Parent Deleted', 'The parent details has been successfully removed.');
            }, function (err) {
                $rootScope.loading = false;
                scope.clearParentDetails();
                scope.getParentList(studentId);

                //$rootScope.modal('Deletion Failed', 'Cannot remove the Parent now. Please try again.');
            });
        }
    };

    scope.updateParent = function (isValid, studentId, parentId) {
        if (isValid) {
            $rootScope.loading = true;
            scope.parentData.studentProfileId = studentId;
            StudentService.updateParentByStudentAndParentId(studentId, parentId, scope.parentData).then(function (response) {
                message.success('Updated Successfully', 'The parent details has been updated.');
                scope.clearParentDetails();
                StudentService.studentGetParentProfilesByStudentId(studentId).then(function (sucResponse) {
                    $rootScope.loading = false;
                    scope.parentList = sucResponse.data;
                    scope.loadParentData(parentId);
                }, function (err) {
                    $rootScope.loading = false;
                    message.error('Retrieval Failed', 'Cannot retrive parent details now. Please try again.');
                });
                scope.commonData.isParentDisabled = true;
            }, function (errorData) {
                $rootScope.loading = false;
                message.error('Update Failed', 'Cannot update the Parent details now. Please try again.');
            });
        } else {
            message.error('Form Incomplete', 'Please fill out all the mandatory fields');
        }
    }
}

studentSearch = function (scope, ele, attr, StudentService, $rootScope, $filter, message, $window) {
    $rootScope.currentPageName = "Search";
    scope.userData = {};
    scope.studentData = {};

    $rootScope.loading = false;

    var isStudentList = JSON.parse($window.localStorage.getItem('searchList'));
    if (isStudentList) {
        scope.studentData.selectCourse = isStudentList.selectCourse;
        scope.studentData.selectClassroom = isStudentList.selectClassroom;
        scope.studentData.searchText = isStudentList.searchText;
        scope.studentSearchList = isStudentList.studentSearchList;
    }

    scope.resetSearchData = function () {
        scope.studentSearchList = null;
    }

    scope.getCourseAndBindClassroom = function () {
        $rootScope.loading = true;
        var selectedCourseId = $rootScope.findKeyInListByValue(scope.$parent.courseMap, scope.studentData.selectCourse);
        StudentService.getClassroomsByCourse(selectedCourseId).then(function (response) {
            $rootScope.loading = false;
            scope.classroomList = response.data;
            console.log('sha', response);
        }, function () {
            $rootScope.loading = false;
            message.error('Classrooms Error', 'Cannot get classrooms for the selected Course.');
        });
    }

    scope.searchStudent = function (isValid) {
        scope.isSearchPressed = false;
        $rootScope.loading = true;
        var isOnlyCourse = scope.studentData.selectCourse && !scope.studentData.selectClassroom && !scope.studentData.searchText ? true : false;
        var isCourseAndClassroom = scope.studentData.selectCourse && scope.studentData.selectClassroom && !scope.studentData.searchText ? true : false;
        var isOnlySearchText = !scope.studentData.selectCourse && !scope.studentData.selectClassroom && scope.studentData.searchText ? true : false;

        if (isOnlyCourse) {
            var courseId = $rootScope.findKeyInListByValue(scope.$parent.courseMap, scope.studentData.selectCourse);
            StudentService.getStudentsListByCourse(courseId).then(function (response) {
                $rootScope.loading = false;
                var searchList = {
                    'selectCourse': scope.studentData.selectCourse,
                    'selectClassroom': scope.studentData.selectClassroom,
                    'searchText': scope.studentData.searchText,
                    'studentSearchList': response.data
                };
                $window.localStorage.setItem('searchList', JSON.stringify(searchList));
                scope.studentSearchList = response.data;
                scope.isSearchPressed = true;
            }, function (err) {
                $rootScope.loading = false;
                message.error('Retrieval Failed', 'Cannot retrive the student\'s list. Please try again');
                scope.isSearchPressed = true;

            });
        } else if (isCourseAndClassroom) {
            var courseId = $rootScope.findKeyInListByValue(scope.$parent.courseMap, scope.studentData.selectCourse);
            var classroomId = $rootScope.findKeyInListByValue(scope.$parent.classroomMap, scope.studentData.selectClassroom);
            StudentService.getStudentsListByClassroom(courseId, classroomId).then(function (response) {
                $rootScope.loading = false;
                var searchList = {
                    'selectCourse': scope.studentData.selectCourse,
                    'selectClassroom': scope.studentData.selectClassroom,
                    'searchText': scope.studentData.searchText,
                    'studentSearchList': response.data
                };
                $window.localStorage.setItem('searchList', JSON.stringify(searchList));
                scope.studentSearchList = response.data;
                scope.isSearchPressed = true;
            }, function (err) {
                $rootScope.loading = false;
                message.error('Retrieval Failed', 'Cannot retrive the student\'s list. Please try again');
                scope.isSearchPressed = true;

            });
        } else if (isOnlySearchText) {
            StudentService.getStudentsListByKeyword(scope.studentData.searchText.toLowerCase()).then(function (response) {
                $rootScope.loading = false;
                var searchList = {
                    'selectCourse': scope.studentData.selectCourse,
                    'selectClassroom': scope.studentData.selectClassroom,
                    'searchText': scope.studentData.searchText,
                    'studentSearchList': response.data
                };
                $window.localStorage.setItem('searchList', JSON.stringify(searchList));
                scope.studentSearchList = response.data;
                scope.isSearchPressed = true;
            }, function (err) {
                $rootScope.loading = false;
                message.error('Retrieval Failed', 'Cannot retrive the student\'s list. Please try again');
                scope.isSearchPressed = true;

            });
        } else {
            $rootScope.loading = false;
            scope.isSearchPressed = true;

            scope.studentData.selectCourse = scope.studentData.selectClassroom = scope.studentData.searchText = null;
            message.error('Invalid selection', 'You should select ONE option out of the two.');
        }

    }
}