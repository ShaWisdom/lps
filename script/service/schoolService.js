﻿(function () {
    angular
        .module('lotusSchoolApp')
        .service('SchoolService', SchoolService);

    SchoolService.$inject = ['$http', 'BASE_PATH'];
    function SchoolService($http, BASE_PATH) {
        var service = {
            getSchoolDetails: getSchoolDetails,
            updateSchoolProfile: updateSchoolProfile,
            getImage : getImage,
            saveImage : saveImage
        };

        return service;

        function getSchoolDetails(data, successFunction, errorFunction) {
            $http.get(BASE_PATH + 'schools/' + data).then(function (response) {
                return successFunction(response.data);
            }, function (response) {
                return errorFunction(response.data);
            })
        }

        function updateSchoolProfile(code,data, successFunction, errorFunction) {
            $http.put(BASE_PATH + 'schools/' + code,data).then(function (response) {
                return successFunction(response.data);
            }, function (response) {
                return errorFunction(response.data);
            })
        }

        function getImage(code, successFunction, errorFunction) {
            $http.get(BASE_PATH + 'schools/LOTUS/fileupload?fileType=SCHOOL_NAME_PICTURE&fileIdentifier=' + code).then(function (response) {
                return successFunction(response.data);
            }, function (response) {
                return errorFunction(response.data);
            })
        }

        function saveImage(data, successFunction, errorFunction) {
            $http.post(data.formAction,data).then(function (response) {
                return successFunction(response.data);
            }, function (response) {
                return errorFunction(response.data);
            })
        }
    }
})();