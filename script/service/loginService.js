﻿(function () {
    angular
        .module('lotusSchoolApp')
        .service('LoginService', LoginService);

    LoginService.$inject = ['$http', 'BASE_PATH'];
    function LoginService($http, BASE_PATH) {
        var service = {
            login: login,
            logOut : logOut
        };

        return service;

        function login(data,successFunction,errorFunction) {
            $http.post(BASE_PATH + 'auth/login', data).then(function (response) {
                return successFunction(response.data);
            }, function (response) {
                return errorFunction(response.data);
            })
        }

        function logOut(data, successFunction, errorFunction) {
            $http.post(BASE_PATH + 'auth/logout', data).then(function (status, response) {
                return successFunction(response);
            }, function (response) {
                return errorFunction(response.data);
            })
        }
    }
})();