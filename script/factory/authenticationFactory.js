﻿(function () {
    angular
        .module('lotusSchoolApp')
        .factory('AuthenticationFactory', AuthenticationFactory);

    AuthenticationFactory.$inject = ['$http','$rootScope', '$localStorage'];
    function AuthenticationFactory($http, $rootScope,$localStorage) {
        var service = {
            isLoggedIn: isLoggedIn,
            CurrentUser: CurrentUser,
            SetUserCredentials: SetUserCredentials,
            ClearCredentials: ClearCredentials,
            getCurrentUser: getCurrentUser
        };

        return service;


        function getCurrentUser() {
            $rootScope.CurrentUser = $localStorage.UserData || {};
            $http.defaults.headers.common['Authorization'] = 'stuvwxyzab'; // $rootScope.CurrentUser.authToken;

        }
        function isLoggedIn() {
            //return true;
            return ($localStorage.UserData) ? true : false;
        };
        function CurrentUser() {
            return $localStorage.UserData || {};
        };
        function SetUserCredentials(user) {
            $rootScope.CurrentUser = user;
            $http.defaults.headers.common['Authorization'] ='stuvwxyzab' // user.authToken; // jshint ignore:line
            $localStorage.UserData = user;
        };
        function ClearCredentials() {
            $rootScope.CurrentUser = null;
            delete $http.defaults.headers.common['Authorization']; // jshint ignore:line
            delete $localStorage.UserData;
        };
    }
})();