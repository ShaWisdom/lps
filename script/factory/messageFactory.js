﻿(function () {
    angular.module('messageModule',['toastr']);

    angular.module('messageModule')
        .factory('message', message);

    message.$inject = ['toastr'];
    function message(toastr) {
        var service = {
            success: success,
            error: error,
            info: info,
            warning: warning
        };

        return service;


        function success(title, message) {
            toastr.success(message, title);
        };
        function error(title, message) {
            toastr.error(message, title);
        };
        function info(title, message) {
            toastr.info(message, title);
        };
        function warning(title, message) {
            toastr.warning(message, title);
        };
    }
})();