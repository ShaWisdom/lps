﻿
(function () {
    angular
    .module('lotusSchoolApp')
    .run(run)
    .directive('datepicker', datepicker)
    .directive('autocomplete', autocomplete)
    .directive('getDynamicHeight', getDynamicHeight)
    ;

    function run($rootScope, $state, $http, AuthenticationFactory, $uibModal, $filter, Months) {
        $rootScope.$on('$stateChangeStart', function (e, toState, toParams, fromState, fromParams) {

            if (toState.data.requiredlogin && AuthenticationFactory.isLoggedIn()) {
               // $rootScope.CurrentUser = AuthenticationFactory.CurrentUser();
               // $http.defaults.headers.common['Authorization'] = $rootScope.CurrentUser.authToken; // jshint ignore:line 
           }
           else if (toState.data.requiredlogin) {
            e.preventDefault();
            $state.transitionTo("login");
        }
    });

        $rootScope.goToRoute = function (route) {
            $state.go(route);
        };

        $rootScope.filterSeconds = function (input) {
            var args = Array.prototype.slice.call(arguments, 2),
            momentObj = moment(input);
            return momentObj.format('MM/DD/YYYY h:mm a');
        };

        $rootScope.bloodGroupList = [
        {'id': 0, 'name': 'A - Positive'},
        {'id': 1, 'name': 'A - Negative'},
        {'id': 2, 'name': 'B - Positive'},
        {'id': 3, 'name': 'B - Negative'},
        {'id': 4, 'name': 'AB - Positive'},
        {'id': 5, 'name': 'AB - Negative'},
        {'id': 6, 'name': 'O - Positive'},
        {'id': 7, 'name': 'O - Negative'}
        ];

        $rootScope.relationshipList = [
        {'id': 0, 'name': 'Mother'},
        {'id': 1, 'name': 'Father'},
        {'id': 2, 'name': 'Relative'},
        {'id': 3, 'name': 'Family Friend'},
        {'id': 4, 'name': 'Other'}
        ];

        $rootScope.extraActList = ['Dance', 'Arts', 'Singing', 'Cricket', 'NCC'];

        $rootScope.imageFileType = {
            'student': 'STUDENT_PROFILE_PICTURE',
            'emp': 'EMPLOYEE_PROFILE_PICTURE',
            'school_logo': 'SCHOOL_LOGO_PICTURE',
            'school_name': 'SCHOOL_NAME_PICTURE'
        };

        $rootScope.headers = {
            'Content-Type': 'application/json',
            'Authorization': $rootScope.token
        }

        $rootScope.loading = false;

        $rootScope.findValueInListByKey = function(list, searchElement){
            var flag = true, found = null;
            if(flag) {
                angular.forEach(list, function(value, key){
                    if(key == searchElement){
                        flag = false;
                        found = value;
                    }
                });
            }
            return found;
        };

        $rootScope.findKeyInListByValue = function(list, text){
            var flag = true, found = null;
            if(flag) {
                angular.forEach(list, function(value, key){
                    if(value.toLowerCase() == text.toLowerCase()){
                        flag = false;
                        found = key;
                    }
                });
            }
            return found;
        };

        $rootScope.imageFileType = {
            'student': 'STUDENT_PROFILE_PICTURE',
            'emp': 'EMPLOYEE_PROFILE_PICTURE',
            'school_logo': 'SCHOOL_LOGO_PICTURE',
            'school_name': 'SCHOOL_NAME_PICTURE'
        };

        $rootScope.goToRoute = function (route) {
            $state.go(route);
        };

        $rootScope.buttonImages = {
            'add': 'fa fa-plus-circle fa-2x',
            'save': 'fa fa-floppy-o fa-2x',
            'edit': 'fa fa-pencil-square-o fa-2x',
            'cancel': 'fa fa-times-circle fa-2x',
            'delete': 'fa fa-trash-o fa-2x'
        };

        $rootScope.dateFilterBackend = function(inputDate){
            return $filter('date')(new Date(inputDate), 'yyyy-MM-dd');
        };

        $rootScope.dateFilterFrontend = function(inputDate){
            var date = new Date(inputDate);
            var day = date.getDate() < 10 ? "0" + date.getDate() : date.getDate();
            var month = Months[date.getMonth()];
            var year = date.getFullYear();
            return day + "-" + month + "-" + year;
        };
    }

        function getDynamicHeight($window) {
            return {
                replace: true,
                link: function(scope, ele, attr) {
                    ele.css({'min-height': $window.innerHeight - 50 + 'px'});
                }
            }
        };

//Datepicker directive
function datepicker() {
    return {
        restrict: 'A',
        link: function (scope, element, attrs, ngModelCtrl) {
            $(element).datepicker({
                // dateFormat: 'yy-mm-dd',
                dateFormat: 'dd-MM-yy',
                changeMonth: true,
                changeYear: true,
                yearRange: "-50:+0",
                onSelect: function (date) {
                    scope.$apply();
                }
            });
        }
    };
};
    //End Datepicker directive

    // Autocomplete Directive
    function autocomplete($timeout) {
      return {
        scope: {
          nameList: '='
      },
      link: function(scope, iElement, iAttrs) {
          //scope.names = ["john", "bill", "charlie", "robert", "alban", "oscar", "marie", "celine", "brad", "drew", "rebecca", "michel", "francis", "jean", "paul", "pierre", "nicolas", "alfred", "gerard", "louis", "albert", "edouard", "benoit", "guillaume", "nicolas", "joseph"];


          scope.split = function(val) {
            return val.split(/,\s*/);
        };

        scope.extractLast = function(term) {
            return scope.split(term).pop();
        };

        iElement.bind("keydown", function(event) {
            if (event.keyCode === $.ui.keyCode.TAB && $(this).autocomplete("instance").menu.active) {
                event.preventDefault();
            }
        });
        iElement.autocomplete({
            minLength: 0,
            source: function(request, response) {
          // delegate back to autocomplete, but extract the last term
          response($.ui.autocomplete.filter(
            // scope.names, scope.extractLast(request.term)));
            scope.nameList, scope.extractLast(request.term)));
      },
        //source: scope[iAttrs.uiItems],
        focus: function() {
          // prevent value inserted on focus
          return false;
      },
      select: function(event, ui) {
          $timeout(function() {
            iElement.trigger('input');
        }, 0);
          var terms = scope.split(this.value);
          // remove the current input
          terms.pop();
          // add the selected item
          terms.push(ui.item.value);
          // add placeholder to get the comma-and-space at the end
          terms.push("");
          this.value = terms.join(", ");
          return false;
      }
  });
    }
}
}
    // End AutoComplete Directive

})();