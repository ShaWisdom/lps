(function () {
	'use strict';
	angular
	.module('lotusSchoolApp', ['ui.router', 'ui.bootstrap', 'ngAnimate', 'ngAria', 'ngCookies', 'ngStorage', 'ngMessages', 'chart.js', 'uiSwitch', 'messageModule', 'studentModule', 'datatables', 'datatables.bootstrap'])
	.constant('BASE_PATH', 'http://ec2-52-34-242-83.us-west-2.compute.amazonaws.com:8080/SchoolAdminWS/services/')
	.constant('SERVICE_END_POINTS', 
	{
		"student": 
		{
			"profile": "schools/<schoolCd>/students/profiles/<studentId>",
			"parentDetails": "schools/<schoolCd>/students/<studentId>/parents/<parentId>",
			"search": {
				"course": "schools/<schoolCd>/courses/<courseId>/students", 
				"classroom": "schools/<schoolCd>/courses/<courseId>/classrooms/<classroomId>/students",
				"keyword": "schools/<schoolCd>/students/search?keywords=<keyword>"
			}
		}, 
		"image": 
		{
			"imageUpload": "schools/<schoolCd>/fileupload?fileType=<imageFileType.student>&fileIdentifier=<studentId>"
		},
		"utililty" : {
			"getStates": "util/states?country=India"
		}
	})
	.constant('Months', ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"])
	;

})();