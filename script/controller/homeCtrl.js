(function () {
    angular
        .module('lotusSchoolApp')
        .controller('HomeController', HomeController);

    HomeController.$inject = ['$scope'];
    function HomeController($scope) {
        var vm = this;
        vm.Title = "Home page";

        vm.labels = ["Hardik", "Smith", "John", "Stark", "Captian"];
        vm.series = ['Last Month', 'Current Month'];
        vm.data = [
          [65, 59, 80, 81, 56],
          [28, 48, 40, 86, 90]
        ];
        vm.StudentMarks = {};
        vm.StudentMarks.labels = ["Maths", "Science", "English", "SS", "Hindi"];
        vm.StudentMarks.data = [50,70, 45, 75, 63];
      
    }
})();