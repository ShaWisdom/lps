﻿(function () {
    angular
        .module('lotusSchoolApp')
        .controller('SchoolController', SchoolController);

    SchoolController.$inject = ['$scope', '$state', '$filter', '$rootScope', 'SchoolService', 'message'];
    function SchoolController($scope, $state, $filter, $rootScope, SchoolService, message) {
        var vm = this;
        vm.schoolCd = $rootScope.CurrentUser.schoolCd;
        vm.getSchoolDetails = function (schoolCode) {
            SchoolService.getSchoolDetails(schoolCode, function (data) {
                vm.SchoolDetails = data;
                vm.SchoolDetails.tempIsActive = vm.SchoolDetails.isActive === 'Y';
                vm.SchoolDetails.tempCreatedTs = $filter('date')(vm.SchoolDetails.createdTs, 'mm/dd/yyyy');
                vm.SchoolDetails.tempUpdatedTs = $filter('date')(vm.SchoolDetails.updatedTs, 'mm/dd/yyyy');

                angular.forEach($rootScope.CurrentUser.employeeMap, function (value, key) {
                    if (key == vm.SchoolDetails.updatedBy) {
                        vm.SchoolDetails.displayUpdatedBy = value;
                    }
                });

                vm.tempSchoolDetails = angular.copy(vm.SchoolDetails);

            }, function (error) {
                message.error('Oops...', 'Something went wrong!');
            });
        }

        function calculateYears() {
            var year = new Date().getFullYear();
            vm.Years = [];
            vm.Years.push((year - 1).toString());
            vm.Years.push(year.toString());
            for (var i = 1; i <= 2; i++) {
                vm.Years.push((year + i).toString());
            }
        }

        calculateYears();

        function getLicenceType() {
            vm.LicenceTypes = ["Trial Version", "Monthly Subscription", "Annual Subscription"];
        }

        getLicenceType();

        function getStateList() {
            vm.States = ["India", "China", "UK"];
        }

        getStateList();

        vm.Tabs = [
            { Text: "Profile", Route: "school.profile", Active: true },
            { Text: "Subject", Route: "school.subject", Active: true },
            { Text: "Courses", Route: "school.courses", Active: true },
        ];

        vm.go = function (route) {
            $state.go(route);
        };

        //File Uploading

        vm.changeLogo = function (image) {
            $("#logoUpload").click();
        };
        $scope.logoUploaded = function (file) {
            if (file.files.length > 0) {
                var f = file.files[0];
                if (f.type.indexOf('image/') > -1) {
                    var reader = new FileReader();
                    reader.onload = function (event) {
                        vm.SchoolDetails.logoLocation = event.target.result;
                        if ($scope.$root.$$phase != '$apply' && $scope.$root.$$phase != '$digest') {
                            $scope.$apply();
                        }
                        SchoolService.getImage(vm.SchoolDetails.schoolCd, function (data) {
                            data.file = event.target.result;
                            SchoolService.saveImage(data, function (data) {
                                debugger;
                            }, function (err) {
                                console.log(err);
                            })

                        }, function (err) {
                            console.log(err);
                        });
                    }
                    reader.readAsDataURL(f);
                }
            }
        }

        vm.SaveSchoolProfile = function () {
            if (vm.frmSchoolProfile.$valid) {
                vm.SchoolDetails.isActive = vm.SchoolDetails.tempIsActive ? 'Y' : 'N';
                delete vm.SchoolDetails.tempIsActive;
                delete vm.SchoolDetails.tempCreatedTs;
                delete vm.SchoolDetails.tempUpdatedTs;
                SchoolService.updateSchoolProfile(vm.schoolCd, vm.SchoolDetails, function (data) {
                    message.success('School Profile', 'Saved Successfully!');
                    vm.SchoolDetails.tempIsActive = vm.SchoolDetails.isActive === 'Y';
                    vm.SchoolDetails.tempCreatedTs = $filter('date')(vm.SchoolDetails.createdTs, 'HH:mm');
                    vm.SchoolDetails.tempUpdatedTs = $filter('date')(vm.SchoolDetails.updatedTs, 'mm/dd/yyyy');
                }, function (error) {
                    message.error('Oops...', 'Something went wrong!');
                });
            }
            else {
                message.error('Error', 'Please enter valid data');
            }
        };

        vm.ClearSchoolProfile = function () {
            vm.SchoolDetails = angular.copy(vm.tempSchoolDetails);
        };

        vm.NextTab = function () {
            //vm.SchoolDetails = vm.tempSchoolDetails;
        };

        vm.changeName = function (image) {
            $("#nameUpload").click();
        };

        $scope.nameUploaded = function (file) {
            if (file.files.length > 0) {
                var f = file.files[0];
                if (f.type.indexOf('image/') > -1) {
                    var reader = new FileReader();
                    reader.onload = function (event) {
                        vm.SchoolDetails.nameImageLocation = event.target.result;
                        if ($scope.$root.$$phase != '$apply' && $scope.$root.$$phase != '$digest') {
                            $scope.$apply();
                        }
                    }
                    reader.readAsDataURL(f);
                }
            }
        }

        //Get Initial Data
        vm.getSchoolDetails(vm.schoolCd);
    }
})();