﻿(function () {
    angular
        .module('lotusSchoolApp')
        .controller('MainController', MainController);

    MainController.$inject = ['$scope', '$cookieStore', '$rootScope', '$state','AuthenticationFactory'];
    function MainController($scope, $cookieStore, $rootScope,$state, AuthenticationFactory) {
        var vm = this;
        vm.Title = "Main Page";
      
        var mobileView = 992;
        AuthenticationFactory.getCurrentUser();
        vm.getWidth = function () {
            return window.innerWidth;
        };

        $scope.$watch(function watchWidth(scope) {
             return vm.getWidth;
        }, function (newValue, oldValue) {
                if (newValue() >= mobileView) {
                    if (angular.isDefined($cookieStore.get('toggle'))) {
                        vm.toggle = !$cookieStore.get('toggle') ? false : true;
                    } else {
                        vm.toggle = true;
                    }
                } else {
                    vm.toggle = false;
                }
        });

        vm.toggleSidebar = function () {
            vm.toggle = !vm.toggle;
            $cookieStore.put('toggle', vm.toggle);
        };

        window.onresize = function () {
            $scope.$apply();
        };

        vm.logOut = function () {
            AuthenticationFactory.ClearCredentials();
            window.location.href = "/";
            //$state.go('login');
        }
    }
})();