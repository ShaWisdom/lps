﻿(function () {
    angular
        .module('lotusSchoolApp')
        .controller('StudentController', StudentController);

    StudentController.$inject = ['$scope', '$state'];
    function StudentController($scope, $state) {
        var vm = this;
        vm.Name = "Hardik";

        vm.StudentDetails = {
            profileImage : 'images/student.png'
        }


        vm.Tabs = [
            { Text: "Profile", Route: "student.profile", Active: true },
            { Text: "Parent's Info", Route: "student.parentdetails", Active: false },
            { Text: "Fees", Route: "student.fees", Active: true },
            { Text: "Makrs", Route: "student.marks", Active: true },
            { Text: "Attendance", Route: "student.attendance", Active: false },
            { Text: "Feedback", Route: "student.feedback", Active: true },
        ];

        vm.go = function (route) {
            $state.go(route);
        };
    }
})();