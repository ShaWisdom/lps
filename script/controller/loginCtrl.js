﻿(function () {
    angular
        .module('lotusSchoolApp')
        .controller('LoginController', LoginController);

    LoginController.$inject = ['$scope', '$state', 'LoginService', 'AuthenticationFactory'];
    function LoginController($scope, $state, LoginService, AuthenticationFactory) {
        var vm = this;
        vm.dataLoading = false;

        function initPage() {
            vm.invalid = false;
            AuthenticationFactory.ClearCredentials();
        };
        initPage();

        vm.User = {};
        vm.Login = function () {
            vm.dataLoading = true;
            LoginService.login(vm.User, function ( data) {
                vm.dataLoading = false;
                vm.invalid = false;
                AuthenticationFactory.SetUserCredentials(data);
                $state.go('home');
            }, function (errorMessage) {
                vm.dataLoading = false;
                vm.invalidMessage = errorMessage.errorDescription;
                vm.invalid = true;
            });
        };
    }
})();