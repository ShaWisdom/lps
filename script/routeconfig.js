﻿
(function () {
    angular
    .module('lotusSchoolApp')
    .config(config);

    function config($stateProvider, $urlRouterProvider, $httpProvider) {
        delete $httpProvider.defaults.headers.common['X-Requested-With'];
        //TODO For Future Use
        //$httpProvider.defaults.headers.post['Accept'] = 'application/json, text/javascript';
        //$httpProvider.defaults.headers.post['Content-Type'] = 'application/json; charset=utf-8';
        //$httpProvider.defaults.headers.post['Access-Control-Max-Age'] = '1728000';
        //$httpProvider.defaults.headers.common['Access-Control-Max-Age'] = '1728000';
        $httpProvider.defaults.headers.common['Accept'] = 'application/json, text/javascript';
        $httpProvider.defaults.headers.common['Content-Type'] = 'application/json; charset=utf-8';

        $stateProvider
        .state('home', {
            url: '/home',
            templateUrl: 'view/home.html',
            controller: "HomeController",
            controllerAs: 'home',
            data: {
                requiredlogin: true
            }
        })
        .state("login", {
            url: '/login',
            templateUrl: 'view/login.html',
            controller: "LoginController",
            controllerAs: 'login',
            data: {
                requiredlogin: false
            }
        })

        /*Student Route*/
        .state('student', {
            url: '/student',
            templateUrl: 'app/components/student/student.html',
            abstract: true,
            controller: 'StudentController',
            data: {
                requiredlogin: true
            }
        })

        .state('student.profile', {
            // url: '/profile',
            url: '/profile/:studentId',
            templateUrl: 'app/components/student/_student_profile.html',
            // params: {
            //     studentId: null,
            // },
        })
        .state('student.search', {
            url: '/search',
            templateUrl: 'app/components/student/_student_search.html'
        })
        .state('student.create', {
            url: '/create',
            templateUrl: 'app/components/student/_student_create.html'
        })
        .state('student.parentdetails', {
            url: '/parentdetails',
            templateUrl: 'app/components/student/_student_parent_details.html'
        })
        .state('student.parentdetailscreate', {
            url: '/parentdetails/create',
            templateUrl: 'app/components/student/_student_parent_create.html'
        })
            // .state('student', {
            //     url: '/student',
            //     templateUrl: 'view/student/student.html',
            //     abstract: true,
            //     controller: "StudentController",
            //     controllerAs: 'student',
            //     data: {
            //         requiredlogin: true
            //     }
            // })
            // .state('student.profile', {
            //     url: '/profile',
            //     templateUrl: 'view/student/student.profile.html'
            // })
            // .state('student.parentdetails', {
            //     url: '/parentdetails',
            //     templateUrl: 'view/student/student.parentdetails.html'
            // })
            // .state('student.marks', {
            //     url: '/marks',
            //     templateUrl: 'view/student/student.marks.html'
            // })
            // .state('student.fees', {
            //     url: '/fees',
            //     templateUrl: 'view/student/student.fees.html'
            // })
            // .state('student.attendance', {
            //     url: '/attendance',
            //     templateUrl: 'view/student/student.attendance.html'
            // })
            // .state('student.feedback', {
            //     url: '/feedback',
            //     templateUrl: 'view/student/student.feedback.html'
            // })


        //School Route
        .state('school', {
            url: '/school',
            templateUrl: 'view/school/school.html',
            abstract: true,
            controller: "SchoolController",
            controllerAs: 'school',
            data: {
                requiredlogin: true
            }
        })
        .state('school.profile', {
            url: '/profile',
            templateUrl: 'view/school/school.profile.html'
        })


        $urlRouterProvider.otherwise('/login');
    }
})();